# Home

Introducción básica a Python

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/python_intro), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/python_intro
```

## Contenidos

```{tableofcontents}
```